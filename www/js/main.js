$(document).ready(function(){
    //init nette ajax
    $.nette.init();

    initLibraries();
});

$.nette.ext('initLibraries', {
    complete: function () {
        initLibraries();
    }
});

function initLibraries() {
    $('.datepicker').datepicker({
        language: 'cs',
        format: 'dd.mm.yyyy'
    });
}