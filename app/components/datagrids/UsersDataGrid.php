<?php

namespace App\AdminModule\DataGrids;

use App\Model\Entity\User;
use App\Model\Repository\UserRepository;
use Ublaboo\DataGrid\DataGrid;

class UsersDataGrid extends AbstractDataGrid
{
    /** @var UserRepository */
    private $userRepository;

    /**
     * UsersDataGrid constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @return DataGrid
     */
    public function create()
    {
        $grid = parent::create();
        $grid->setDefaultSort(array('name' => 'ASC'));

        $grid->setDataSource($this->userRepository->getQB());

        $grid->addColumnText('name', 'Jméno')
            ->setSortable()
            ->setFilterText();

        $grid->addColumnDateTime('birthday', 'Datum narození')
            ->setAlign('left')
            ->setSortable()
            ->setFilterDate();

        $grid->addColumnText('superpower', 'Superschopnost')
            ->setSortable()
            ->setFilterText();

        $grid->addAction('edit', 'Editovat', 'edit!')
            ->setIcon('pencil')
            ->setClass('btn btn-xs btn-primary ajax');

        $grid->addAction('delete', 'Smazat')
            ->setIcon('trash')
            ->setClass('btn btn-xs btn-danger')
            ->setConfirm(function(User $item) {
                return "Opravdu chcete odstranit kosmonauta {$item->getName()}?";
            });

        return $grid;
    }
}
