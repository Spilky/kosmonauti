<?php

namespace App\AdminModule\Forms;

use App\Model\Entity\User;
use App\Model\Repository\UserRepository;
use DateTime;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Exception;
use Nette\Application\LinkGenerator;
use Nette\Application\UI\Form;
use Nette\Mail\Message;
use Nette\Mail\SmtpMailer;
use Nette\Object;
use Nette\Security\Passwords;
use Nette\Utils\Random;
use Tracy\Debugger;

class AddEditUserFormFactory extends Object
{
    /** @var UserRepository */
    private $userRepository;
    /** @var bool */
    private $newUser = false;
    /** @var array */
    private $defaultValues = [];

    public function __construct(
        UserRepository $userRepository
    ) {
        $this->userRepository = $userRepository;
    }

    /**
     * @return bool
     */
    public function isNewUser()
    {
        return $this->newUser;
    }

    /**
     * @param array $defaultValues
     */
    public function setDefaultValues($defaultValues)
    {
        $this->defaultValues = $defaultValues;
    }

    /**
     * @return Form
     */
    public function create()
    {
        $form = new Form;

        $form->addText('name', 'Jméno:')
            ->setRequired('Vložte prosím jméno.');

        $control = $form->addText('birthday', 'Datum narození:');
        $control->addCondition(Form::FILLED,'Vložte prosím datum narození.');

        $html = $control->getControlPrototype();
        $html->class[] = 'datepicker';
        $html->data('date-end-date', (new DateTime())->format('d.m.Y'));

        $form->addText('superpower', 'Superschopnost:')
            ->setRequired('Vložte prosím superschopnost.');

        $form->addSubmit('submit', 'Uložit');

        $form->addHidden('id');

        $form->setDefaults($this->defaultValues);

        $form->onSuccess[] = array($this, 'formSucceeded');
        return $form;
    }
    
    /**
     * @param Form $form
     * @param $values
     */
    public function formSucceeded(Form $form, $values)
    {
        $values['birthday'] = new DateTime($values['birthday']);
        if (empty($values['id'])) {
            $user = new User($values);
            $this->newUser = true;
        }

        try {
            if (isset($user)) {
                $this->userRepository->insert($user);
            } else {
                $id = $values['id'];
                unset($values['id']);
                $this->userRepository->updateWhere($values, array('id' => $id));
            }
        } catch (Exception $e) {
            $form->addError($e->getMessage());
            Debugger::log($e);
        }
    }
}
