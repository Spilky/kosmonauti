<?php

namespace App\Presenters;

use App\AdminModule\DataGrids\UsersDataGrid;
use App\AdminModule\Forms\AddEditUserFormFactory;
use App\Model\Repository\UserRepository;
use Nette;
use Nette\Application\BadRequestException;
use Nette\Application\UI\Form;
use Tracy\Debugger;
use Ublaboo\DataGrid\DataGrid;


class HomepagePresenter extends Nette\Application\UI\Presenter
{
    /** @var UserRepository @inject */
    public $userRepository;
    /** @var AddEditUserFormFactory @inject */
    public $addEditUserFormFactory;
    /** @var UsersDataGrid @inject */
    public $usersDataGrid;

    /**
     * @param int $id
     * @throws BadRequestException
     */
    public function actionDelete($id = 0)
    {
        $user = $this->userRepository->getById($id);
        if (is_null($user)) {
            throw new BadRequestException;
        }

        try {
            $this->userRepository->delete($user);
            $this->flashMessage('Uživatel úspěšně smazán.', 'success');
        } catch (\Exception $e) {
            $this->flashMessage('Uživatele se nepodařilo smazat. Zkuste to znovu prosím.', 'danger');
            Debugger::log($e);
        }

        $this->redirect('default');
    }

    public function handleEdit($id = 0)
    {
        $user = $this->userRepository->getById($id);
        if (is_null($user)) {
            throw new BadRequestException;
        }

        $defaultValues = $user->getAsArray();
        $defaultValues['birthday'] = $defaultValues['birthday']->format('d.m.Y');
        $this->addEditUserFormFactory->setDefaultValues($defaultValues);
        $this->template->editUser = true;
        $this->redrawControl('addEditUserModalForm');
    }

    /**
     * @return Form
     */
    protected function createComponentAddEditUserForm()
    {
        $form = $this->addEditUserFormFactory->create();
        $form->onSuccess[] = function ($form) {
            $this->flashMessage(
                'Uživatel byl úspěšně ' . ($this->addEditUserFormFactory->isNewUser() == 'add' ?  'vložen.' : 'editován.'),
                'success'
            );
            $this->redirect('this');
        };
        $form->onError[] = function ($form) {
            $this->flashMessage(
                'Uživatele se nepodařilo ' .
                ($this->addEditUserFormFactory->isNewUser() == 'pridat' ?  'vložit.' : 'editovat.') .
                ' Zkuste to prosím znovu.'
                , 'danger');
        };
        return $form;
    }

    /**
     * @return DataGrid
     */
    protected function createComponentUsersDataGrid()
    {
        $grid = $this->usersDataGrid->create();
        return $grid;
    }
}
